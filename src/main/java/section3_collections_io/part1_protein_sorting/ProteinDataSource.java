package section3_collections_io.part1_protein_sorting;

import java.util.List;

public interface ProteinDataSource {
    List<Protein> getAllProteins();
}
